package eu.execom.w1.workshop1app.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import eu.execom.w1.workshop1app.R;

public class NewUserActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_user);
    }
}
