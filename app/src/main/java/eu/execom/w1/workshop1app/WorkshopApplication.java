package eu.execom.w1.workshop1app;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;

/**
 * Created by Alex on 4/29/17.
 */

public class WorkshopApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // initialize Fresco library
        Fresco.initialize(this);
    }
}
