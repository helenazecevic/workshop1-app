package eu.execom.w1.workshop1app.protocol;

import eu.execom.w1.workshop1app.model.User;

/**
 * Created by Alex on 4/29/17.
 */

public interface UserSelectedListener {

    void userSelected(User user);
}
