package eu.execom.w1.workshop1app.generic;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Alex on 4/29/17.
 */

public class ViewWrapper<V extends View> extends RecyclerView.ViewHolder {

    private V view;

    public ViewWrapper(V itemView) {
        super(itemView);
        view = itemView;
    }

    public V getView() {
        return view;
    }
}