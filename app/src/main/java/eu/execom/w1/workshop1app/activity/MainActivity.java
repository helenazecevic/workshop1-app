package eu.execom.w1.workshop1app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import eu.execom.w1.workshop1app.R;
import eu.execom.w1.workshop1app.adapter.UserAdapter;
import eu.execom.w1.workshop1app.dao.mock.UserDao;
import eu.execom.w1.workshop1app.model.User;
import eu.execom.w1.workshop1app.protocol.UserSelectedListener;

public class MainActivity extends AppCompatActivity implements UserSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final RecyclerView recyclerView = (RecyclerView)
                findViewById(R.id.recycler_view);

        // recycler view can stack items vertically or horizontally
        recyclerView.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));

        final UserAdapter adapter = new UserAdapter(this, UserDao.fetchUsers(), this);
        recyclerView.setAdapter(adapter);

        final FloatingActionButton fab =
                (FloatingActionButton) findViewById(R.id.fab);

        // set listener on FAB that starts new activity
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intent =
                        new Intent(MainActivity.this, NewUserActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void userSelected(User user) {

    }
}
